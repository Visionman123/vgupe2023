# VGUPE2023

VGU Programming Exercise

## Description:
[Module description](VGU-Module-Description.pdf)

## Team 1: Online Library Management
### Members
 1. 17822 Lý Nam Sơn (Leader)
 2. 17675 Nguyễn Văn Khánh Nghi
 3. 17700 Nguyễn Phạm Kiên Trung
 4. 17137 Hồ Vĩnh Hào
 5. 17159 Lê Hoàng Thiên Quang
 6. 17544 Viên Kim Lân
 7. 17851 Huỳnh Minh Triết


## Team 2:
### Members:
 1. Phan Chí Thọ (17232)
 2. Trần Nguyễn Minh Quân (17640)
 3. Lê Hoàng Kim Thanh (18047)(Leader)
 4. Nguyễn Lê Anh Quân (18875)
 5. Phạm Nguyễn Đan Quỳnh (17937) 
 6. Lê Hoàng Đăng Nguyên (17028)
 7. Vương Khánh Linh (18070)
 8. Hoàng Minh Thông (17995)


## Team 3: Online Library Management
### Members:
 1. Tra Dang Khoa- 13789744 (team leader)
 2. Son Nguyen Thanh - 13789908
 3. Dong Do Dinh - 13789722
 4. Quang Nguyen Huu Minh - 13789726
 5. Mai Trọng Nhân - 13794792
 6. Tran Hoang Kim - 13796562
 7. Võ Nguyễn Minh Duy (He) - 13796504


 ## Team 4
 ### Members:
 1. Trần Trung Nghĩa - 17420 - @trungnghia17420 (leader)
 2. Phạm Lê Hoàng Duy - 17590 - @17590
 3. Nguyễn Phan Trúc Ly - 18715 - @18715
 4. Trần Đăng Khoa - 15713 - @FinnK30062001
 5. Vũ Duy Thành - 17712 - @rickyvu2311
 6. Võ Đăng Khoa - 18955 - @GoodGuyBin


 ## Team 5
 - 
